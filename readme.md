Introduction to Docker Compose
--------------------------------------
A simple way to start up several Docker containers at once is by using Docker Compose. Specify configurations in a ```docker-compose.yml``` file, navigate to the location of the file in a terminal and run ```docker-compose up```. This then spins up all the containers included in the file. To stop all containers, just run ```docker-compose down``` in the same location.

A very basic ```docker-compose.yml``` file can look like the following:
```
#!yaml
my_container:
  image: ubuntu:14.04
  command: echo hi
```

In this file, ```my_container``` can be any arbitrary name and the ```image``` command specifies which image to use, in this case ```ubuntu:14.04```. If such an image is not present locally, Docker will attempt to pull the image from Docker Hub. The ```command``` command specifies what command to run in the container. 

In case one wants to start up two containers, the ```docker-compose.yml``` file could look something like this:

```
#!yaml
container1:
  image: europeanspallationsource/centos7.1.1503-tini
  command: echo hi

container2:
  image: centos:7.1.1503
  command: echo bye
```

Running ```docker-compose up``` in the directory of the file would then start up two containers, ```container1``` using ```europeanspallationsource/centos7.1.1503-tini``` and ```container2``` using ```centos:7.1.1503```. In case you want to run even more containers, just add more entries to the file.

Let's add more specifications to our ```docker-compose.yml``` file. If we would like to, for example, make use of some directory on the host inside the container, we could use the ```volumes``` specification. If we furthermore would like to make some environment variables available in the container, we could use the 
```environment``` command. Let's also add some ```ports``` to use for communication with the container. The file could look something like this:

```
#!yaml
my_container:
  image: europeanspallationsource/centos7.1.1503-tini
  volumes:
    - '/path/on/host:/path/in/container'
    - '~/different/path/on/host:/different/path/on/container:ro'
  environment:
    - PREFIX=pre1
    - OTHER_PREFIX=pre2
  ports:
    - "5064:5064"
    - "5064:5064/udp"
    - "2056:5000"
```

The ports are on the form ```"HOST:CONTAINER"```. When using ```volumes```, one can specify permissions. In the last example, the container can only read the files in  ```~/different/path/on/host``` but can do anything to the files in ```/path/on/host```.

Assume that on the image ```our-image```, we have a script ```ourscript.sh``` located in ```/usr/bin``` and on the image ```their-image``` we have a script ```theirscript.sh``` located in ```/opt```. In order to start up two containers, executing one script each in containers with the names ```container1``` and ```container2```, we would create a ```docker-compose.yml``` file looking like:

```
#!yaml
our_container:
  image: our-image
  container_name: container1
  command: /usr/bin/ourscript.sh

their_container:
  image: their-image
  container_name: container2
  command: /opt/theirscript.sh
```

If no container names are specified, they will be automatically generated.

This short introduction hopefully gave some insight into how Docker Compose can be used. A description of all of the available specifications can be found here: https://docs.docker.com/compose/compose-file/.

We now move on to how to use Docker Compose to run IOCs and simulators.


--------------------------------------
Starting up several IOCs and their corresponding simulators at once (EEE installed from tarball)
--------------------------------------
This section provides a checklist to follow in order to run several IOCs and their corresponding simulators in one container per IOC. The Docker image which will be used is ```europeanspallationsource/eee-tarball-tini```, which has CentOS7.1.1503 as base and EEE installed from a tarball. We will later discuss how to create new images with EEE installed from different tarballs. 

We will describe how to set up an individual container, repeat for every container you want to include in the ```docker-compose.yml``` file. 

1. If needed, edit your ```*.cmd``` and ```*.db``` files so they can handle a PREFIX passed at runtime of the container. A simple example of a ```*.cmd``` file could look something like this:

        require modbus
        require modbus_one

        epicsEnvSet("PREFIX", ${PREFIX})  #  using the PREFIX environment variable
        drvAsynIPPortConfigure("tcpcon1","localhost:502",0,0,1)
        modbusInterposeConfig("tcpcon1", 0, 0, 0)
        drvModbusAsynConfigure("examplereadcoils", "tcpcon1", 0, 1, 1, 1, 0, 1000, "Test")

        dbLoadRecords(modbus_one.db, "PREFIX=${PREFIX}")  #  using the PREFIX environment variable
with the corresponding ```*.db``` file:

        record(bi, "${PREFIX}:coil1") {  #  using the PREFIX environment variable to name the PV
          field(SCAN, "I/O Intr")
          field(DTYP, "asynUInt32Digital")
          field(INP,  "@asynMask(examplereadcoils 0 1)")
          field(ZNAM, "Low")
          field(ONAM, "High")
        }

1. Make sure that all IOC and simulator files are present. They are one of the following:
    * already on the image.
    * copied onto a new image which is used instead of the original one. This can be done by creating a ```Dockerfile```, using ```europeanspallationsource/eee-tarball-tini``` as the base and then using the COPY or ADD command to add the files and then building this new image. 
    * mounted into the container from the host. Use the ```volumes``` command in the ```docker-compose.yml``` file to do this.

2. Create a startup script which starts up all the simulators and the IOC and make it available in the container (see previous item). A simple one could look something like 
       
        #!/bin/bash
        cd /path/to/simulators
        /start_simulators.sh &
        cd /path/to/ioc
        make
        iocsh st.cmd
In case several IOCs are to be run in the same container, procServ can be used to run them in the background. The script would then look something like

        #!/bin/bash
        cd /path/to/simulators
        /start_simulator1.sh &
        /start_simulator2.sh &
        cd /path/to/ioc1
        make
        procServ <some_port> ${EPICS_ENV_PATH}/iocsh st.cmd
        cd /path/to/ioc2
        make
        procServ <some_port> ${EPICS_ENV_PATH}/iocsh st.cmd
        wait     
The ```wait``` command is there to keep the container running until all subprocesses have finished. This is needed since a Docker container exits if nothing is running in the foreground. 

3. Create the ```docker-compose.yml``` file. Some things should be present:
    * ```environment``` should include a ```PREFIX``` variable. This can then be used in the IOC startup script.
    * ```stdin_open: true``` and ```tty: true``` should be included if ```iocsh``` is being used to run the IOC. This allows the user to interact with the IOCshell and prevents the container from exiting.

The containers are now ready to be started!

Once the containers are up and running, ```bash``` in any of the containers can be accessed with the command ```docker exec -it <container> /bin/bash```, where ```<container>``` is either the ```container_name``` or the ```container_id```. The ```container_name``` and ```container_id``` can be found by checking the list of Docker containers by running ```docker ps```.

In order to speed up the startup and increase portability of the containers, building a new image could be a good solution. This new image could contain all the needed files, and even have everything compiled. In case any compilation is done, the environment variables need to be sourced manually. This can be done by including ```. /etc/profile.d/ess_epics_env.sh``` in the same ```RUN``` command that is used to do the compilation. See the Siemens PLC example ```Dockerfile``` for an example.

There are three examples in the ```docker/examples``` directory of this repo, one with a Siemens PLC simulator, one with a Modbus simulator and one with the Chopper simulator.

--------------------------------------
Starting up several IOCs and their corresponding simulators at once (EEE mounted via nfs)
--------------------------------------
Follow the steps in the previous section, with a few changes and additions to the ```docker-compose.yml``` file:

* Change the ```image``` to ```europeanspallationsource/eee-nfs-tini```
* Include the ```privileged: true``` specification for all containers. This is needed to be able to mount via nfs.

See the Modbus example in the ```docker/examples``` directory of this repo for an example.

--------------------------------------
Potential pitfalls
--------------------------------------
* If the IOC is started only using ```iocsh```, accessing the ```iocsh```ell is difficult at best. Hence, using procServ is preferable.
* Don't forget to add a ```wait``` to the end of the startup script. Otherwise, the container will exit quickly.
* In case the ```europeanspallationsource/eee-nfs-tini``` image is used, compilation of the modules cannot be done until the container is started.
* In case the ```europeanspallationsource/eee-tarball-tini``` image is used, the range of included modules is limited. For example the ```modbus``` module is not present.
--------------------------------------
Structure of IOCs and their simulators
--------------------------------------
With the intent of having a common standard, I propose that the directory of the IOC which is run in the container should contain a ```simulator``` subdirectory. It should contain the startup script for the simulators themselves as well as a startup script to be executed upon spinning up the container.

--------------------------------------
One container running N IOCs vs N containers running one IOC each
--------------------------------------

Docker containers running IOCs are in general very light weight. On the MacBook Air I'm using there is no problem running 200 or even 300 containers, each running a simple simulator and IOC. About the same amount of IOCs and simulators can be run if all of them are run in one container.

However, if the IOCs and simulators are grouped in some way, more than double the total amount of IOCs could be run. On the MacBook Air I'm using, it's possible to run up to 1000 IOCs and their simulators, running 20 containers with 50 IOCs and their simulators per container.

This suggests that in order to use containers to simulate more complex systems, segmenting the IOCs over different containers in a clever way might be needed.

In the case of less complex systems, both using one container per IOC or one container with all IOCs are valid options. However, I suggest using one container per IOC and its corresponding simulators. Besides being more in line with the Docker philosophy of one process per container, the main reason for this is that it is easy to make changes to your system when you use Docker Compose. For example, this allows for using different images for different IOCs or mount different directories from the host, and if one of the IOC needs to be tweaked, it's easy to just change or replace that IOC's entry in the ```docker-compose.yml``` file.

--------------------------------------
Creating new base image with EEE installed from a tarball
--------------------------------------
The Docker image ```europeanspallationsource/eee-tarball-tini``` is created by using the Dockerfile found in this repository under ```docker/base_images/tini/tarball_tini```. It installs EEE from a tarball which will become outdated. In order to create a new image which installs EEE from a different tarball, the following steps must be made:

1. Edit the mentioned Dockerfile. A few changes must be made. 
        
    * The three subsequent lines
                
            curl -O https://artifactory01.esss.lu.se/artifactory/EEE/epics_environment_150821.tar.bz2 && \
            sudo tar xvf epics_environment_150821.tar.bz2 -C / && \
            rm -f epics_environment_150821.tar.bz2 && \
        must be edited to use the new tarball.

    * In case you want to keep the environment variable script ```/etc/profile.d/ess_epics_env.sh``` included in the new tarball, remove the two lines
                
            rm -f /etc/profile.d/ess_epics_env.sh && \
and 
            
            COPY src/eee/ess_epics_env.sh /etc/profile.d/ess_epics_env.sh
If you leave them as they are, the image will use EEE version 1.8.1.
    
    * In case EEE version 1.8.1 is included in the new tarball or you for some reason do not want to include this in the new image, remove the line
            
            COPY src/eee/1.8.1/ /opt/epics/modules/environment/1.8.1/
but take care on how you deal with the environment variable script mentioned in the previous item.

2. Build the new image. Open up a terminal and navigate to the ```docker/base_images/tini/tarball_tini``` directory. Run ```docker build -t europeanspallationsource/eee-tarball-tini .```. Don't miss the period ```.``` at the end.

3. (Optional) Push the new image to Docker Hub. Open up a terminal and run ```docker push europeanspallationsource/eee-tarball-tini```.

--------------------------------------
Julabo water bath simulator
--------------------------------------
This repository also contains a simulator for the Julabo water bath. The simulator is produced using the Kameleon framework and can be found in the ```julabo``` directory. More on Kameleon can be found here: https://bitbucket.org/europeanspallationsource/kameleon.