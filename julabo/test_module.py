# -*- coding: utf-8 -*-
"""
Created on Mon Aug  1 16:06:59 2016

@author: erikackzell

Test script for the Julabo F25-HL simulator.

Start up the simulator and run this script in a terminal. Restart the simulator
before every time this script is run.

The tests prints its to the terminal.

The test sends commands to the simulator (Kameleon server) and expects certain
values to be returned.
"""
from __future__ import division
import pexpect
import os


print("Testing the Julabo F25-HL simulator...")
#  The COMMANDS list contains all the commands being sent to the simulator as
#  well as the output each of the commands are expected to return.
COMMANDS = [["OUT_SP_03 33.234", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_03 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_03 -20.2", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_SP_03 300", ""],
            ["IN_SP_03", "300.0"],
            ["OUT_SP_04 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_04 39.123", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_04 324.2", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_SP_04 -40", ""],
            ["IN_SP_04", "-40.0"],
            ["OUT_MODE_01 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_MODE_01 2.3", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_MODE_01 8", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_MODE_01 -3", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_MODE_01 0", ""],
            ["IN_MODE_01", "0"],
            ["OUT_MODE_02 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_MODE_02 2.3", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_MODE_02 1", "WARNING CODE 09 >COMMAND NOT ALLOWED IN CURRENT OPERATING MODE<"],
            ["OUT_MODE_02 -3", "WARNING CODE 09 >COMMAND NOT ALLOWED IN CURRENT OPERATING MODE<"],
            ["OUT_MODE_02 0", ""],
            ["IN_MODE_02", "0"],
            ["OUT_MODE_03 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_MODE_03 2.3", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_MODE_03 5", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_MODE_03 -5", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_MODE_03 0", ""],
            ["IN_MODE_03", "0"],
            ["OUT_MODE_04 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_MODE_04 2.3", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_MODE_04 5", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_MODE_04 -5", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_MODE_04 0", ""],
            ["IN_MODE_04", "0"],
            ["OUT_MODE_05 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_MODE_05 2.3", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_MODE_05 2", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_MODE_05 -5", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_MODE_05 1", ""],
            ["IN_MODE_05", "1"],
            ["OUT_MODE_08 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_MODE_08 2.3", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_MODE_08 2", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_MODE_08 -5", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_MODE_08 0", ""],
            ["IN_MODE_08", "0"],
            ["OUT_SP_00 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_00 22.233", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_00 510.2", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_SP_00 -502.11", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_SP_00 -105", ">WARNING : VALUE EXCEEDS TEMPERATURE LIMITS<"],
            ["OUT_SP_00 305.12", ">WARNING : VALUE EXCEEDS TEMPERATURE LIMITS<"],
            ["OUT_SP_00 12.34", ""],
            ["IN_SP_00", "12.34"],
            ["OUT_SP_01 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_01 22.233", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_01 510.2", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_SP_01 -502.11", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_SP_01 -105", ">WARNING : VALUE EXCEEDS TEMPERATURE LIMITS<"],
            ["OUT_SP_01 305.12", ">WARNING : VALUE EXCEEDS TEMPERATURE LIMITS<"],
            ["OUT_SP_01 43.1", ""],
            ["IN_SP_01", "43.1"],
            ["OUT_SP_02 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_02 22.233", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_02 510.2", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_SP_02 -502.11", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_SP_02 -105", ">WARNING : VALUE EXCEEDS TEMPERATURE LIMITS<"],
            ["OUT_SP_02 305.12", ">WARNING : VALUE EXCEEDS TEMPERATURE LIMITS<"],
            ["OUT_SP_02 133.92", ""],
            ["IN_SP_02", "133.92"],
            ["OUT_SP_06 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_06 22.233", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_06 110.2", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_SP_06 -102.11", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_SP_06 54.23", ""],
            ["OUT_SP_06 54.23", ""],
            ["OUT_SP_07 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_07 2.3", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_SP_07 5", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_SP_07 0", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_SP_07 1", ""],
            ["OUT_PAR_04 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_04 4.23", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_04 5.4", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_PAR_04 -0.5", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_PAR_04 3.2", ""],
            ["OUT_PAR_06 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_06 4.23", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_06 100.2", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_PAR_06 0.0", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_PAR_06 33.2", ""],
            ["IN_PAR_06", "33.2"],
            ["OUT_PAR_07 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_07 4.2", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_07 10000", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_PAR_07 2", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_PAR_07 655", ""],
            ["IN_PAR_07", "655"],
            ["OUT_PAR_08 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_08 4.2", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_08 1000", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_PAR_08 -1", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_PAR_08 51", ""],
            ["IN_PAR_08", "51"],
            ["OUT_PAR_09 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_09 41.82", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_09 100", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_PAR_09 0", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_PAR_09 99.2", ""],
            ["IN_PAR_09", "99.2"],
            ["OUT_PAR_10 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_10 41.82", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_10 100", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_PAR_10 0.5", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_PAR_10 12", ""],
            ["IN_PAR_10", "12.0"],
            ["OUT_PAR_11 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_11 4.2", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_11 10000", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_PAR_11 2", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_PAR_11 423", ""],
            ["IN_PAR_11", "423"],
            ["OUT_PAR_12 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_12 4.2", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_12 1000", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_PAR_12 -1", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_PAR_12 876", ""],
            ["IN_PAR_12", "876"],
            ["OUT_PAR_13 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_13 4.2", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_13 600", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_PAR_13 -600", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_PAR_13 234", ""],
            ["IN_PAR_13", "234"],
            ["OUT_PAR_14 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_14 4.2", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_14 600", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_PAR_14 -600", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_PAR_14 -32", ""],
            ["IN_PAR_14", "-32"],
            ["OUT_PAR_15 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_15 4.2", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_15 210", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_PAR_15 -210", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_PAR_15 15", ""],
            ["IN_PAR_15", "15"],
            ["OUT_PAR_16 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_16 4.2", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_PAR_16 210", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_PAR_16 -210", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_PAR_16 5", ""],
            ["IN_PAR_16", "5"],
            ["OUT_HIL_00 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_HIL_00 4.2", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_HIL_00 1", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_HIL_00 -110", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_HIL_00 -82", ""],
            ["IN_HIL_00", "82"],
            ["OUT_HIL_01 blah", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_HIL_01 4.2", "WARNING CODE 08 >INVALID COMMAND<"],
            ["OUT_HIL_01 110", "WARNING CODE 11 >VALUE TOO LARGE<"],
            ["OUT_HIL_01 0", "WARNING CODE 10 >VALUE TOO SMALL<"],
            ["OUT_HIL_01 12", ""],
            ["IN_HIL_01", "12"],
            ["VERSION", "software_version"],
            ["STATUS", ""],
            ["IN_PV_00", ""],
            ["IN_PV_01", ""],
            ["IN_PV_02", ""],
            ["IN_PV_03", ""],
            ["IN_PV_04", "200"],
            ["IN_SP_05", ""],
            ["OUT_MODE_05 0", ""],
            ["IN_SP_07", "1"],
            ["OUT_MODE_05 1", ""],
            ["IN_SP_07", ""],
            ["IN_SP_08", ""],
            ["IN_PAR_00", ""],
            ["IN_PAR_01", ""],
            ["IN_PAR_02", ""],
            ["IN_PAR_03", ""],
            ["IN_PAR_04", ""],
            ["IN_PAR_05", "{}".format(round(82 / 12, 2))]]

telconn = pexpect.spawnu('telnet localhost 9999')   #  connecting to kameleon via telnet
tmp_file = open('.tmp_test', 'w')   #  temporary file to use for server output
telconn.logfile = tmp_file
success = True

for i in range(len(COMMANDS)):
    command = COMMANDS[i][0]    #  command to send
    expected = COMMANDS[i][1]   #  expected return
    telconn.send('{}\r'.format(command))    #  sending the command to kameleon
    try:
        telconn.expect('{}\r'.format(expected), timeout=5)  #  waiting until expected value is returned
    except: #  if expected value is not returned
        success = False
        break

tmp_file.close()
os.remove('.tmp_test')

if success:
    print("Success")
else:
    print("Failure (make sure the simulator was restarted prior to the test)")
