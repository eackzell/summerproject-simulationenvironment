# -*- coding: utf-8 -*-
"""
Created on Thu Aug  4 15:09:59 2016

@author: erikackzell

Script to easily generate new basic docstrings for the functions in the Julabo
simulator.
"""

import shutil
import os

#  COMMANDS list copied from the Julabo .kam file
COMMANDS = [["Set temperature setpoint number", "OUT_MODE_01***", 1],
            ["Set selftuning mode", "OUT_MODE_02***", 2],
            ["Set external programmer input mode", "OUT_MODE_03***", 3],
            ["Set control mode", "OUT_MODE_04***", 4],
            ["Start/stop the unit", "OUT_MODE_05***", 5],
            ["Set control dynamics", "OUT_MODE_08***", 6],
            ["Set temperature setpoint 1", "OUT_SP_00***", 7],
            ["Set temperature setpoint 2", "OUT_SP_01***", 8],
            ["Set temperature setpoint 3", "OUT_SP_02***", 9],
            ["Set high temperature warning limit", "OUT_SP_03***", 10],
            ["Set low temperature warning limit", "OUT_SP_04***", 11],
            ["Set manipulated variable for the heater via serial interface", "OUT_SP_06***", 12],
            ["Set the pump pressure stage", "OUT_SP_07***", 13],
            ["Set CoSpeed for external control", "OUT_PAR_04***", 14],
            ["Set Xp control parameter of the internal controller", "OUT_PAR_06***", 15],
            ["Set Tn control parameter of the internal controller", "OUT_PAR_07***", 16],
            ["Set Tv control parameter of the internal controller", "OUT_PAR_08***", 17],
            ["Set Xp control parameter of the cascade controller", "OUT_PAR_09***", 18],
            ["Set proportional portion of the cascade controller", "OUT_PAR_10***", 19],
            ["Set Tn control parameter of the cascade controller", "OUT_PAR_11***", 20],
            ["Set Tv control parameter of the cascade controller", "OUT_PAR_12***", 21],
            ["Set maximal internal temperature of the cascade controller", "OUT_PAR_13***", 22],
            ["Set minimum internal temperature of the cascade controller", "OUT_PAR_14***", 23],
            ["Set band limit (upper)", "OUT_PAR_15***", 24],
            ["Set band limit (lower)", "OUT_PAR_16***", 25],
            ["Set the desired maximum cooling power", "OUT_HIL_00***", 26],
            ["Set the desired maximum heating power", "OUT_HIL_01***", 27],
            ["Get number of sofware version", "VERSION", 28],
            ["Get status message, error message", "STATUS", 29],
            ["Get actual bath temperature", "IN_PV_00", 30],
            ["Get heating power being used", "IN_PV_01", 31],
            ["Get temperature value registered by the external Pt100 sensor", "IN_PV_02", 32],
            ["Get temperature value registered by the safety sensor", "IN_PV_03", 33],
            ["Get setpoint temperature of the excess temperature protection", "IN_PV_04", 34],
            ["Get working temperature setpoint 1", "IN_SP_00", 35],
            ["Get working temperature setpoint 2", "IN_SP_01", 36],
            ["Get working temperature setpoint 3", "IN_SP_02", 37],
            ["Get high temperature warning limit", "IN_SP_03", 38],
            ["Get low temperature warning limit", "IN_SP_04", 39],
            ["Get setpoint temperature of the external programmer", "IN_SP_05", 40],
            ["Get 1. Adjusted pump stage in the OFF condition. 2. Pump stage corresponding to the effective rotation speed after start.",
                     "IN_SP_07", 41],
            ["Get value of a flowrate sensor connected to the E-Prog input", "IN_SP_08", 42],
            ["Get temperature difference between working sensor and safety senson", "IN_PAR_00", 43],
            ["Get Te - Time constant of the external bath", "IN_PAR_01", 44],
            ["Get Si - Internal slope", "IN_PAR_02", 45],
            ["Get Ti - Time constant of the internal bath", "IN_PAR_03", 46],
            ["Get CoSpeed - Band limit (max diff between temperatures in internal bath and external system)",
                     "IN_PAR_04", 47],
            ["Get factor pk/ph0: Ratio of max cooling capacity vs max heating capacity",
                     "IN_PAR_05", 48],
            ["Get Xp control parameter of the internal controller", "IN_PAR_06", 49],
            ["Get Tn control parameter of the internal controller", "IN_PAR_07", 50],
            ["Get Tv control parameter of the internal controller", "IN_PAR_08", 51],
            ["Get Xp control parameter of the cascade controller", "IN_PAR_09", 52],
            ["Get proportional portion of the cascade controller", "IN_PAR_10", 53],
            ["Get Tn control parameter of the cascade controller", "IN_PAR_11", 54],
            ["Get Tv control parameter of the cascade controller", "IN_PAR_12", 55],
            ["Get adjusted maximum internal temperature of the cascade controller",
                     "IN_PAR_13", 56],
            ["Get adjusted minimum internal temperature of the cascade controller",
                     "IN_PAR_14", 57],
            ["Get band limit (upper)", "IN_PAR_15", 58],
            ["Get band limit (lower)", "IN_PAR_16", 59],
            ["Get current setpoint", "IN_MODE_01", 60],
            ["Get selftuning type", "IN_MODE_02", 61],
            ["Get type of external programmer input", "IN_MODE_03", 62],
            ["Get internal/external temperature control", "IN_MODE_04", 63],
            ["Get circulator in stop/start condition", "IN_MODE_05", 64],
            ["Get adjusted control dynamics", "IN_MODE_08", 65],
            ["Get maximum cooling power", "IN_HIL_00", 66],
            ["Get maximum heating power", "IN_HIL_01", 67]]

#  Just to get rid of warnings
CUSTOM, FIXED = None, None

#  STATUSES list copied from Julabo .kam file
STATUSES = [["Set temperature setpoint number", CUSTOM, "set_current_setpoint()"],
            ["Set selftuning mode", CUSTOM, "set_selftuning_mode()"],
            ["Set external programmer input mode", CUSTOM, "set_external_programmer_input_mode()"],
            ["Set control mode", CUSTOM, "set_control_mode()"],
            ["Start/stop the unit", CUSTOM, "set_power()"],
            ["Set control dynamics", CUSTOM, "set_control_dynamics()"],
            ["Set temperature setpoint 1", CUSTOM, "set_setpoint1()"],
            ["Set temperature setpoint 2", CUSTOM, "set_setpoint2()"],
            ["Set temperature setpoint 3", CUSTOM, "set_setpoint3()"],
            ["Set high temperature warning limit", CUSTOM, "set_high_temperature_warning_limit()"],
            ["Set low temperature warning limit", CUSTOM, "set_low_temperature_warning_limit()"],
            ["Set manipulated variable for the heater via serial interface", CUSTOM, "set_manipulated_variable_serial_interface()"],
            ["Set the pump pressure stage", CUSTOM, "set_pump_pressure_stage()"],
            ["Set CoSpeed for external control", CUSTOM, "set_cospeed_for_external_control()"],
            ["Set Xp control parameter of the internal controller", CUSTOM, "set_xp_control_parameter_internal_controller()"],
            ["Set Tn control parameter of the internal controller", CUSTOM, "set_tn_control_parameter_internal_controller()"],
            ["Set Tv control parameter of the internal controller", CUSTOM, "set_tv_control_parameter_internal_controller()"],
            ["Set Xp control parameter of the cascade controller", CUSTOM, "set_xp_control_parameter_cascade_controller()"],
            ["Set proportional portion of the cascade controller", CUSTOM, "set_proportional_portion_cascade_controller()"],
            ["Set Tn control parameter of the cascade controller", CUSTOM, "set_tn_control_parameter_cascade_controller()"],
            ["Set Tv control parameter of the cascade controller", CUSTOM, "set_tv_control_parameter_cascade_controller()"],
            ["Set maximal internal temperature of the cascade controller", CUSTOM, "set_max_internal_temperature_cascade_controller()"],
            ["Set minimum internal temperature of the cascade controller", CUSTOM, "set_min_internal_temperature_cascade_controller()"],
            ["Set band limit (upper)", CUSTOM, "set_upper_band_limit()"],
            ["Set band limit (lower)", CUSTOM, "set_lower_band_limit()"],
            ["Set the desired maximum cooling power", CUSTOM, "set_max_cooling_power()"],
            ["Set the desired maximum heating power", CUSTOM, "set_max_heating_power()"],
            ["Get number of sofware version", FIXED, "software_version"],
            ["Get status message, error message", CUSTOM, "get_msg()"],
            ["Get actual bath temperature", CUSTOM, "get_bath_temperature()"],
            ["Get heating power being used", CUSTOM, "get_used_heating_power()"],
            ["Get temperature value registered by the external Pt100 sensor", CUSTOM, "get_temperature_external_Pt100()"],
            ["Get temperature value registered by the safety sensor", CUSTOM, "get_temperature_safety_sensor()"],
            ["Get setpoint temperature of the excess temperature protection", CUSTOM, "get_setpoint_excess_temperature_protection()"],
            ["Get working temperature setpoint 1", CUSTOM, "get_setpoint1()"],
            ["Get working temperature setpoint 2", CUSTOM, "get_setpoint2()"],
            ["Get working temperature setpoint 3", CUSTOM, "get_setpoint3()"],
            ["Get high temperature warning limit", CUSTOM, "get_high_temperature_warning_limit()"],
            ["Get low temperature warning limit", CUSTOM, "get_low_temperature_warning_limit()"],
            ["Get setpoint temperature of the external programmer", CUSTOM, "get_setpoint_temperature_external_programmer()"],
            ["Get 1. Adjusted pump stage in the OFF condition. 2. Pump stage corresponding to the effective rotation speed after start.",
                     CUSTOM, "get_pump_stage()"],
            ["Get value of a flowrate sensor connected to the E-Prog input", CUSTOM, "get_flowrate_external_programmer()"],
            ["Get temperature difference between working sensor and safety senson", CUSTOM, "get_difference_working_sensor_safety_sensor()"],
            ["Get Te - Time constant of the external bath", CUSTOM, "get_time_constant_external_bath()"],
            ["Get Si - Internal slope", CUSTOM, "get_internal_slope()"],
            ["Get Ti - Time constant of the internal bath", CUSTOM, "get_time_constant_internal_bath()"],
            ["Get CoSpeed - Band limit (max diff between temperatures in internal bath and external system)",
                     CUSTOM, "get_diff_internal_external()"],
            ["Get factor pk/ph0: Ratio of max cooling capacity vs max heating capacity",
                     CUSTOM, "get_ratio_cooling_heating_capacity()"],
            ["Get Xp control parameter of the internal controller", CUSTOM, "get_xp_control_parameter_internal_controller()"],
            ["Get Tn control parameter of the internal controller", CUSTOM, "get_tn_control_parameter_internal_controller()"],
            ["Get Tv control parameter of the internal controller", CUSTOM, "get_tv_control_parameter_internal_controller()"],
            ["Get Xp control parameter of the cascade controller", CUSTOM, "get_xp_control_parameter_cascade_controller()"],
            ["Get proportional portion of the cascade controller", CUSTOM, "get_proportional_portion_cascade_controller()"],
            ["Get Tn control parameter of the cascade controller", CUSTOM, "get_tn_control_parameter_cascade_controller()"],
            ["Get Tv control parameter of the cascade controller", CUSTOM, "get_tv_control_parameter_cascade_controller()"],
            ["Get adjusted maximum internal temperature of the cascade controller",
                 CUSTOM, "get_adjusted_maximum_internal_temperature_cascade_controller()"],
            ["Get adjusted minimum internal temperature of the cascade controller",
                     CUSTOM, "get_adjusted_minimum_internal_temperature_cascade_controller()"],
            ["Get band limit (upper)", CUSTOM, "get_upper_band_limit()"],
            ["Get band limit (lower)", CUSTOM, "get_lower_band_limit()"],
            ["Get current setpoint", CUSTOM, "get_current_setpoint()"],
            ["Get selftuning type", CUSTOM, "get_selftuning_mode()"],
            ["Get type of external programmer input", CUSTOM, "get_external_programmer_input_mode()"],
            ["Get internal/external temperature control", CUSTOM, "get_control_mode()"],
            ["Get circulator in stop/start condition", CUSTOM, "get_power()"],
            ["Get adjusted control dynamics", CUSTOM, "get_control_dynamics()"],
            ["Get maximum cooling power", CUSTOM, "get_max_cooling_power()"],
            ["Get maximum heating power", CUSTOM, "get_max_heating_power()"]]


#  Saving all functions in list
functions = [L[2] for L in STATUSES]

#  Empty list in which to store the commands for the device
cmds = []

#  Filling the cmds list
for L in COMMANDS:
    if L[1][-3:] == "***":  #  removing *** ending from commands
        cmds.append(L[1][:-3])
    else:
        cmds.append(L[1])

#  Constructing a list of docstrings
docs = ['    """\n    {description}.\n\n    Corresponds to the command {cmd}\n    """\n'.format(description=COMMANDS[i][0], cmd=cmds[i]) for i in range(len(STATUSES))]

#  The input file
infile = "julabo_f25-hl.kam"

#  Creating a backup
backupfile = "julabo_f25-hl.kam.bak"
shutil.copy(infile, backupfile)

#  Loop to write all the docstrings
for i, function in enumerate(functions):
    with open(infile, 'r') as inputfile:
        with open('tmp', 'w') as outputfile:
            for line in inputfile:
                if line.startswith('def {fnc}:'.format(fnc=function)):
                    outputfile.write(line)
                    outputfile.write(docs[i])
                else:
                    outputfile.write(line)
    shutil.copy('tmp', infile)
os.remove('tmp')