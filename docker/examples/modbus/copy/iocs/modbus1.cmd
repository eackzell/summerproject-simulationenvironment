require modbus
require modbus_one
 
# Using the PREFIX environment variable
epicsEnvSet("PREFIX", ${PREFIX})

# Specify the TCP endpoint and give your bus a name eg. "tcpcon1".
drvAsynIPPortConfigure("tcpcon1","localhost:502",0,0,1)
# Configure the interpose for TCP
modbusInterposeConfig("tcpcon1", 0, 0, 0)
 

# Configure the Modbus driver
# drvModbusAsynConfigure(portName, tcpPortName, slaveAddress, modbusFunction,
#                       modbusStartAddress, modbusLength, dataType,
#                       pollMsec, plcType);
# In this example we poll a single bit at register address 1 every 1000ms.
drvModbusAsynConfigure("examplereadcoils", "tcpcon1", 0, 1, 1, 1, 0, 1000, "Test")
 
# Load the database defining your EPICS records, using the PREFIX environment variable
dbLoadRecords(modbus_one.db, "PREFIX=${PREFIX}")
