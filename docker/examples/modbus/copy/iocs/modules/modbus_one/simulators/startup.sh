#!/bin/bash

# Starting the simulator
cd /etc/example/iocs/modules/modbus_one/simulators
./simstart.sh &

# Compiling module
cd /etc/example/iocs/modules/modbus_one
make EXCLUDE_ARCHS=eldk

# Running the IOC
cd /etc/example/iocs
iocsh modbus1.cmd
