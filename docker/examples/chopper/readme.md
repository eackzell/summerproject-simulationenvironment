--------------------------------------------------
An illustrative example of how to use Docker Compose with the Chopper simulator
--------------------------------------------------

In this example, three containers are started. Each of the containers are running a Chopper simulator, with different prefixes. 

In order to run the example, you just have to 

1. **Start the example:** In this directory, run ```docker-compose up```. The output from the containers can then be seen in the terminal.

To stop the containers, run ```docker-compose down```.
