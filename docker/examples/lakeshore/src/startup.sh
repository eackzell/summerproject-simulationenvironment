#!/bin/bash

# Start the simulator
cd /etc/simulators/kameleon
python kameleon.py --file=simulators/lakeshore_336/lakeshore_336.kam &

# Start the IOC
cd /etc/iocs
iocsh LStest-startup.cmd
