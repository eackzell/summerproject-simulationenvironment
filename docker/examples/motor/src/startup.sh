#!/bin/bash -il

# Making the change to st.cmd, allowing the user to select PREFIX at runtime.
sed -i "s/P=IOC/P=$PREFIX/g" /MCAG_setupMotionDemo/m-epics-IcePAP/epicsIOC/iocBoot/iocIcePAP/st.cmd

# Launch motorsim components
caRepeater &

cd /MCAG_setupMotionDemo/m-epics-IcePAP/simulator
./doit.sh &

cd /MCAG_setupMotionDemo/m-epics-IcePAP/epicsIOC
./doit.sh

# Cleanup (We stay in epicsIOC/doit.sh while running)
PID_CAREP=`ps aux | grep caRepeater | grep -v grep | awk '{ print $2 }'`
PID_MOTOR=`ps aux | grep simMotor | grep -v grep | awk '{ print $2 }'`

echo "Killing simMotor PID=$PID_MOTOR"
kill $PID_MOTOR

echo "Killing caRepeater PID=$PID_CAREP"
kill $PID_CAREP
