# -*- coding: utf-8 -*-
"""
Created on Tue Aug  2 16:43:37 2016

@author: erikackzell

This file is used to generate the docker-compose.yml file needed to run the
testbeamline including simulators and corresponding IOCs for the following
devices:

    8 Choppers
    15 Motion axes
    1 Lakeshore module

Comments are included in the code to point out how to make changes if desired,
for example changing the different prefixes.

"""

composefile = open("docker-compose.yml", 'w') #  Opening file to write to

#  Prefixes for the choppers. The same number of chopper containers as the
#  number of prefixes will be created.
chopper_prefs = ['chopper1', 'chopper2', 'chopper3', 'chopper4', 'chopper5',
                 'chopper6', 'chopper7', 'chopper8']


#  Loop to write all the entries to the file. The ports can easily be changed
#  and other configurations can be added to preference. The stdin_open and tty
#  configurations must remain.
for i, pref in enumerate(chopper_prefs):
    composefile.write(
"""
chopper%s:
  image: dmscid/plankton
  tty: true
  stdin_open: true
  container_name: chopper%s
  ports:
    - "500%s:5064"
    - "500%s:5064/udp"
  command: "--parameters pv_prefix=%s:"
""" % (i + 1, i + 1, i + 1, i + 1, pref))


#  Prefixes for the motion axes. The same number of motion axis containers as the
#  number of prefixes will be created.
motion_prefs = ['s1r', 's1l', 's1t', 's1b', 's2r', 's2l', 's2t', 's2b', 's3r',
                's3l', 's3t', 's3b', 'stage_x', 'stage_y', 'stage_rot']

#  Loop to write all the entries to the file. The ports can easily be changed
#  and other configurations can be added to preference. The stdin_open and tty
#  configurations must remain.
for i, pref in enumerate(motion_prefs):
    if i < 9:   #  to use subsequent ports
        composefile.write(
"""
motion%s:
  image: europeanspallationsource/motorsim
  tty: true
  stdin_open: true
  container_name: motion%s
  ports:
    - "600%s:5064"
    - "600%s:5064/udp"
  environment:
    - "PREFIX=%s"
""" % (i + 1, i + 1, i + 1, i + 1, pref))
    else:   #  to use subsequent ports
        composefile.write(
"""
motion%s:
  image: europeanspallationsource/motorsim
  tty: true
  stdin_open: true
  container_name: motion%s
  ports:
    - "60%s:5064"
    - "60%s:5064/udp"
  environment:
    - "PREFIX=%s"
""" % (i + 1, i + 1, i + 1, i + 1, pref))

#  Writing the entry for the Lakeshore module.
composefile.write(
"""
lakeshore:
  image: lakeshore
  tty: true
  stdin_open: true
  container_name: lakeshore
  privileged: true
  ports:
    - "7000:5064"
    - "7000:5064/udp"
  environment:
    - "PREFIX=LS"
""")

composefile.close() #  Closing the file.