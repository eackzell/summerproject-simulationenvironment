--------------------------------------------------
Example running the Testbeamline using Docker Compose
--------------------------------------------------

In this example, the Testbeamline is simulated using Docker Compose. The following containers are run:

* 8 Choppers
* 15 Motion controllers
* 1 Lakeshore module

In order to run the example, the following steps need to be taken:

1. Generate the ```docker-compose.yml``` file by running ```python generate_composefile.py``` in this directory.
2. Build the lakeshore image by navigating to the lakeshore example directory of this repository and running ```docker build -t lakeshore .```.
3. Run ```docker-compose up -d``` in this directory to spin up the containers, letting the containers run in detached mode with the ```-d``` flag. 
4. Stop the containers by running ```docker-compose down``` in this directory.
