require s7plc
require s7plc_one

# Using the PREFIX environment variable 
epicsEnvSet("PREFIX", ${PREFIX})

# s7plcConfigure (PLCname, IPaddr, port, inSize, outSize, bigEndian, recvTimeout, sendIntervall)
s7plcConfigure("PLC1", "127.0.0.1", 1102, 9, 0, 0, 1000, 1000)
 
# Load the database defining your EPICS records and using PREFIX environment variable
dbLoadRecords(s7plc_one.db, "PREFIX=${PREFIX}")
