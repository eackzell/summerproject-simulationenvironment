#!/bin/bash

# Starting the simulator
cd /etc/example/iocs/modules/s7plc_one/simulators
./simstart.sh &

# Starting the IOC
cd /etc/example/iocs
iocsh s7plc1.cmd
