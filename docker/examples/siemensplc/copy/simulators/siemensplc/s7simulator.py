#!/usr/bin/env python

import SocketServer
import select
import struct
import time
import random

HOST="127.0.0.1"
PORT=1102

#Simple fan simulator which takes a control speed between 0-10 and produces
#values for the fan tachometer and bearing temperature.
#
#Data Block sent to EPICS (little-endian):
#0-3	Float	Fan speed (RPM)
#4-7	Float	Bearing Temperature (C)
#8	Byte	Current control speed setting (0-10)
#
#Data Block received from EPICS:
#0	Byte	Control speed setting (0-10)
#
class SimpleFan:
  def __init__(self):
    self.speed = 5

  def serialise(self):
    if self.speed>0:
      rpm=self.speed*100.0+random.uniform(-5,5)
    else:
      rpm=0
    temp=20+self.speed*6.0+random.uniform(-1,1)
    print rpm
    return struct.pack("<ffB", rpm, temp, self.speed)

  def deserialise(self, controlstr):
    vals=struct.unpack("B", controlstr)
    if vals[0]>10:
      self.speed=10
    else:
      self.speed=vals[0]

#Singleton instance of the fan
thefan = SimpleFan()

#TCP handler
class MyTCPHandler(SocketServer.BaseRequestHandler):
  def handle(self):
    self.request.setblocking(0)
    try:
      while True:
        ready = select.select([self.request], [], [], 0.1)
        if ready[0]:
          self.data = self.request.recv(10)
          if len(self.data)>0:
            thefan.deserialise(self.data)

        self.request.sendall(thefan.serialise())

        time.sleep(0.5)
    except:
      #Socket is broken
      pass

server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
server.serve_forever()

