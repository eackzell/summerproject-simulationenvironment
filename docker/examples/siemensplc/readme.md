--------------------------------------------------
An illustrative example of how to use Docker Compose with PLC simulator and IOC
--------------------------------------------------

In this example, two containers are started. Each of the containers are running a PLC simulator and an IOC, using different port and prefix settings. 

In order to run the example, you have to 

1. **Build the image:** Navigate to this directory in your terminal and run 
    ```docker build -t plcexample .```. 
2. **Start the example:** While still in the directory, run ```docker-compose up -d```, where the ```-d``` flag starts up the containers in detached mode.

To see if the modules are running as they should, you can run 
```docker exec -it <container_name> /bin/bash``` to connect to one of the containers and then run ```camonitor ${PREFIX}:fanrpm``` and examine the output.

To stop the containers, run ```docker-compose down```.