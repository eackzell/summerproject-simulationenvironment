#!/bin/sh
# ESS EPICS Environment profile
# Slightly modified version of the file described here:
# https://ess-ics.atlassian.net/wiki/display/HAR/How+to+setup+the+EEE+from+scratch
# Changes are:
# i) This script uses EEE version 1.8.1 instead of 1.4.1
# ii) The following environment variables are added, as needed for the IOCshell to
# work properly:
#    EPICS_CA_AUTO_ADDR_LIST=NO
#    EPICS_CA_ADDR_LIST=127.255.255.255
 
# Pathmunge is a method for adding a path to PATH avoiding to add it multiple times. Copied from /etc/profile
pathmunge () {
    case ":${PATH}:" in
        *:"$1":*)
            ;;
        *)
            if [ "$2" = "after" ] ; then
                PATH=$PATH:$1
            else
                PATH=$1:$PATH
            fi
    esac
}
 
# Set the following environment variables
export EPICS_HOST_ARCH=centos7-x86_64
export EPICS_BASES_PATH=/opt/epics/bases
export EPICS_MODULES_PATH=/opt/epics/modules
export EPICS_ENV_PATH=${EPICS_MODULES_PATH}/environment/1.8.1/3.14.12.5/bin/${EPICS_HOST_ARCH}
export EPICS_BASE=${EPICS_BASES_PATH}/base-3.14.12.5
export EPICS_CA_AUTO_ADDR_LIST=NO
export EPICS_CA_ADDR_LIST=127.255.255.255
 
# Source the bash completions for requireExec
source /opt/epics/modules/environment/1.8.1/3.14.12.5/bin/${EPICS_HOST_ARCH}/requireExecCompletions
 
# Add iocsh to path
pathmunge ${EPICS_ENV_PATH} before
 
# Add EPICS tools to path (caput, caget, ...)
pathmunge ${EPICS_BASE}/bin/${EPICS_HOST_ARCH} before
